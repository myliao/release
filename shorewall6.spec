%define name shorewall6
%define version 5.2.8
%define release 0base

Summary: Shoreline Firewall 6 is an ip6tables-based firewall for Linux systems.
Name: %{name}
Version: %{version}
Release: %{release}
License: GPLv2
Packager: Tom Eastep <teastep@shorewall.net>
Group: Networking/Utilities
Source: %{name}-%{version}.tgz
URL: http://www.shorewall.org/
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-root
Requires: iptables iproute shorewall >= 4.5.0-0
Provides: shoreline_firewall = %{version}-%{release}

%description

The Shoreline Firewall 6, more commonly known as "Shorewall6", is a Netfilter
(ip6tables) based IPv6 firewall that can be used on a dedicated firewall system,
a multi-function gateway/ router/server or on a standalone GNU/Linux system.

%prep

%setup

%build

%install

./configure.pl --host=%{_vendor} \
               --prefix=%{_prefix} \
               --tmpdir=%{_tmpdir} \
               --perllibdir=%{perl_vendorlib} \
               --libexecdir=%{_libexecdir} \
               --sbindir=%{_sbindir}

DESTDIR=%{buildroot} ./install.sh

touch %{buildroot}/etc/shorewall6/isusable
touch %{buildroot}/etc/shorewall6/notrack

%clean
rm -rf $RPM_BUILD_ROOT

%post

if [ $1 -eq 1 ]; then
        if [ -x %{_sbindir}/systemctl ]; then
	        %{_sbindir}/systemctl enable shorewall6
        elif [ -x /usr/bin/systemctl ]; then
	        /usr/bin/systemctl enable shorewall6
	elif [ -x %{_sbindir}/insserv ]; then
		%{_sbindir}/insserv /etc/rc.d/shorewall6
	elif [ -x %{_sbindir}/chkconfig ]; then
		%{_sbindir}/chkconfig --add shorewall6;
	fi
fi

%preun

if [ $1 = 0 ]; then
        if [ -x %{_sbindir}/systemctl ]; then
	        %{_sbindir}/systemctl disable shorewall6
        elif [ -x /usr/bin/systemctl ]; then
	        /usr/bin/systemctl disable shorewall6
	elif [ -x %{_sbindir}/insserv ]; then
		%{_sbindir}/insserv -r %{_initddir}/shorewall6
	elif [ -x %{_sbindir}/chkconfig ]; then
		%{_sbindir}/chkconfig --del shorewall6
	fi

	rm -f /etc/shorewall/startup_disabled

fi

%files
%defattr(0644,root,root,0755)
%attr(0644,root,root) /usr/lib/systemd/system/shorewall6.service
%attr(0755,root,root) %dir /etc/shorewall6
%ghost %(attr 0644,root,root) /etc/shorewall6/isusable
%ghost %(attr 0644,root,root) /etc/shorewall6/masq
%attr(0755,root,root) %dir /usr/share/shorewall6
%attr(0755,root,root) %dir /usr/share/shorewall6/configfiles
%attr(0755,root,root) %dir /usr/share/shorewall6/deprecated
%attr(0700,root,root) %dir /var/lib/shorewall6
%attr(0600,root,root) %config(noreplace) /etc/shorewall6/*

%attr(0755,root,root) %dir /etc/sysconfig/
%attr(0600,root,root) %config(noreplace) /etc/sysconfig/shorewall6

%attr(0644,root,root) /etc/logrotate.d/shorewall6

%attr(-   ,root,root) %{_sbindir}/shorewall6

%attr(0644,root,root) /usr/share/shorewall6/version
%attr(0644,root,root) /usr/share/shorewall6/actions.std
%attr(0644,root,root) /usr/share/shorewall6/action.mangletemplate
%attr(0644,root,root) /usr/share/shorewall6/action.template
%attr(-   ,root,root) /usr/share/shorewall6/functions
%attr(0644,root,root) /usr/share/shorewall6/lib.base
%attr(0644,root,root) /usr/share/shorewall6/macro.*
%attr(0644,root,root) /usr/share/shorewall6/helpers
%attr(0644,root,root) /usr/share/shorewall6/configpath

%attr(0644,root,root) /usr/share/shorewall6/configfiles/*

%attr(0644,root,root) %{_mandir}/man5/*
%attr(0644,root,root) %{_mandir}/man8/*

%doc COPYING INSTALL changelog.txt releasenotes.txt tunnel ipsecvpn ipv6 Samples6

%changelog
* Fri Sep 18 2020 Tom Eastep <teastep@shorewall.net>
- Updated to 5.2.8-0base
* Thu Sep 10 2020 Tom Eastep <teastep@shorewall.net>
- Updated to 5.2.8-0RC1
* Wed Sep 09 2020 Tom Eastep <teastep@shorewall.net>
- Updated to 5.2.8-0Beta2
* Sat Aug 01 2020 Tom Eastep <teastep@shorewall.net>
- Updated to 5.2.8-0Beta1
* Wed Jul 29 2020 Tom Eastep <teastep@shorewall.net>
- Updated to 5.2.7-0base
* Sat Jul 25 2020 Tom Eastep <teastep@shorewall.net>
- Updated to 5.2.7-0RC1
* Tue Jul 07 2020 Tom Eastep <tom@shorewall.net>
- Updated to 5.2.7-0Beta1
* Mon Jun 29 2020 Tom Eastep <tom@shorewall.net>
- Updated to 5.2.6-0base
* Sat Jun 27 2020 Tom Eastep <tom@shorewall.net>
- Updated to 5.2.6-0RC1
* Sun Jun 14 2020 Tom Eastep <tom@shorewall.net>
- Updated to 5.2.6-0Beta1
* Wed Jun 10 2020 Tom Eastep <tom@shorewall.net>
- Updated to 5.2.5-0base
* Sat Jun 06 2020 Tom Eastep <tom@shorewall.net>
- Updated to 5.2.5-0RC1
* Wed Jun 03 2020 Tom Eastep <tom@shorewall.net>
- Updated to 5.2.5-0Beta2
* Sun Apr 19 2020 Tom Eastep <tom@shorewall.net>
- Updated to 5.2.5-0Beta1
* Sat Mar 14 2020 Tom Eastep <tom@shorewall.net>
- Updated to 5.2.4-0RC1
* Fri Mar 06 2020 Tom Eastep <tom@shorewall.net>
- Updated to 5.2.4-0Beta1
* Mon Feb 11 2019 Tom Eastep tom@shorewall.net
- Updated to 5.2.3-0base
* Wed Feb 06 2019 Tom Eastep tom@shorewall.net
- Updated to 5.2.3-0RC1
* Sun Feb 03 2019 Tom Eastep tom@shorewall.net
- Updated to 5.2.3-0Beta2
* Tue Jan 22 2019 Tom Eastep tom@shorewall.net
- Updated to 5.2.3-0Beta1
* Wed Jan 16 2019 Tom Eastep tom@shorewall.net
- Updated to 5.2.2-0base
* Tue Jan 08 2019 Tom Eastep tom@shorewall.net
- Updated to 5.2.2-0RC1
* Fri Jan 04 2019 Tom Eastep tom@shorewall.net
- Updated to 5.2.2-0Beta2
* Tue Oct 02 2018 Tom Eastep tom@shorewall.net
- Updated to 5.2.2-0Beta1
* Fri Sep 28 2018 Tom Eastep tom@shorewall.net
- Updated to 5.2.1-0base
* Thu Aug 23 2018 Tom Eastep tom@shorewall.net
- Updated to 5.2.1-0RC1
* Tue Jul 03 2018 Tom Eastep tom@shorewall.net
- Updated to 5.2.1-0Beta3
* Mon Jul 02 2018 Tom Eastep tom@shorewall.net
- Updated to 5.2.1-0Beta2
* Sat Apr 21 2018 Tom Eastep tom@shorewall.net
- Updated to 5.2.1-0Beta1
* Wed Apr 11 2018 Tom Eastep tom@shorewall.net
- Updated to 5.2.0-1
* Wed Mar 28 2018 Tom Eastep tom@shorewall.net
- Updated to 5.2.0-0base
* Mon Mar 26 2018 Tom Eastep tom@shorewall.net
- Updated to 5.2.0-0RC2
* Tue Feb 27 2018 Tom Eastep tom@shorewall.net
- Updated to 5.2.0-0RC1
* Wed Feb 21 2018 Tom Eastep tom@shorewall.net
- Updated to 5.2.0-0Beta2
* Wed Jan 24 2018 Tom Eastep tom@shorewall.net
- Updated to 5.2.0-0Beta1
* Tue Jan 23 2018 Tom Eastep tom@shorewall.net
- Updated to 5.1.12-0Beta2
* Wed Jan 17 2018 Tom Eastep tom@shorewall.net
- Updated to 5.1.12-0Beta1
* Wed Jan 10 2018 Tom Eastep tom@shorewall.net
- Updated to 5.1.11-0base
* Fri Jan 05 2018 Tom Eastep tom@shorewall.net
- Updated to 5.1.11-0RC1
* Sun Dec 31 2017 Tom Eastep tom@shorewall.net
- Updated to 5.1.11-0Beta2
* Tue Dec 26 2017 Tom Eastep tom@shorewall.net
- Updated to 5.1.11-0Beta1
* Sat Dec 23 2017 Tom Eastep tom@shorewall.net
- Updated to 5.1.10-0base
* Mon Dec 18 2017 Tom Eastep tom@shorewall.net
- Updated to 5.1.10-0RC2
* Sat Dec 09 2017 Tom Eastep tom@shorewall.net
- Updated to 5.1.10-0RC1
* Fri Dec 01 2017 Tom Eastep tom@shorewall.net
- Updated to 5.1.10-0Beta2
* Wed Nov 22 2017 Tom Eastep tom@shorewall.net
- Updated to 5.1.10-0Beta1
* Wed Nov 15 2017 Tom Eastep tom@shorewall.net
- Updated to 5.1.9-0base
* Sat Nov 11 2017 Tom Eastep tom@shorewall.net
- Updated to 5.1.9-0RC1
* Fri Nov 03 2017 Tom Eastep tom@shorewall.net
- Updated to 5.1.9-0Beta2
* Thu Oct 19 2017 Tom Eastep tom@shorewall.net
- Updated to 5.1.9-0Beta1
* Sun Oct 15 2017 Tom Eastep tom@shorewall.net
- Updated to 5.1.8-0base
* Tue Oct 10 2017 Tom Eastep tom@shorewall.net
- Updated to 5.1.8-0RC1
* Sat Oct 07 2017 Tom Eastep tom@shorewall.net
- Updated to 5.1.8-0Beta2
* Mon Sep 18 2017 Tom Eastep tom@shorewall.net
- Updated to 5.1.8-0Beta1
* Mon Sep 18 2017 Tom Eastep tom@shorewall.net
- Updated to 5.1.7-0base
* Sun Sep 17 2017 Tom Eastep tom@shorewall.net
- Updated to 5.1.7-0RC2
* Fri Sep 01 2017 Tom Eastep tom@shorewall.net
- Updated to 5.1.7-0RC1
* Wed Aug 23 2017 Tom Eastep tom@shorewall.net
- Updated to 5.1.7-0Beta2
* Tue Aug 22 2017 Tom Eastep tom@shorewall.net
- Updated to 5.1.7-0Beta1
* Wed Aug 16 2017 Tom Eastep tom@shorewall.net
- Updated to 5.1.6-0base
* Tue Aug 15 2017 Tom Eastep tom@shorewall.net
- Updated to 5.1.6-0RC2
* Tue Aug 15 2017 Tom Eastep tom@shorewall.net
- Updated to 5.1.6-0RC1
* Wed Aug 09 2017 Tom Eastep tom@shorewall.net
- Updated to 5.1.6-0RC1
* Thu Aug 03 2017 Tom Eastep tom@shorewall.net
- Updated to 5.1.6-0Beta2
* Thu Jul 20 2017 Tom Eastep tom@shorewall.net
- Updated to 5.1.6-0Beta1
* Mon Jun 26 2017 Tom Eastep tom@shorewall.net
- Updated to 5.1.5-0base
* Wed Jun 21 2017 Tom Eastep tom@shorewall.net
- Updated to 5.1.5-0RC1
* Fri Jun 16 2017 Tom Eastep tom@shorewall.net
- Updated to 5.1.5-0Beta2
* Thu May 11 2017 Tom Eastep tom@shorewall.net
- Updated to 5.1.5-0Beta1
* Fri May 05 2017 Tom Eastep tom@shorewall.net
- Updated to 5.1.4-0base
* Mon Apr 24 2017 Tom Eastep tom@shorewall.net
- Updated to 5.1.4-0RC1
* Fri Mar 24 2017 Tom Eastep tom@shorewall.net
- Updated to 5.1.4-0Beta2
* Mon Mar 13 2017 Tom Eastep tom@shorewall.net
- Updated to 5.1.4-0Beta1
* Mon Mar 13 2017 Tom Eastep tom@shorewall.net
- Updated to 5.1.3-0base
* Sun Mar 12 2017 Tom Eastep tom@shorewall.net
- Updated to 5.1.3-0RC2
* Fri Mar 10 2017 Tom Eastep tom@shorewall.net
- Updated to 5.1.3-0RC1
* Mon Mar 06 2017 Tom Eastep tom@shorewall.net
- Updated to 5.1.3-0Beta2
* Wed Feb 22 2017 Tom Eastep tom@shorewall.net
- Updated to 5.1.3-0Beta1
* Fri Feb 17 2017 Tom Eastep tom@shorewall.net
- Updated to 5.1.2-0RC1
* Fri Feb 10 2017 Tom Eastep tom@shorewall.net
- Updated to 5.1.2-0Beta2
* Wed Jan 25 2017 Tom Eastep tom@shorewall.net
- Updated to 5.1.2-0Beta1
* Tue Jan 24 2017 Tom Eastep tom@shorewall.net
- Updated to 5.1.1-0base
* Thu Jan 19 2017 Tom Eastep tom@shorewall.net
- Updated to 5.1.1-0RC1
* Tue Jan 17 2017 Tom Eastep tom@shorewall.net
- Updated to 5.1.1-0Beta2
* Wed Jan 11 2017 Tom Eastep tom@shorewall.net
- Updated to 5.1.1-0Beta1
* Tue Jan 10 2017 Tom Eastep tom@shorewall.net
- Updated to 5.1.0-1
* Wed Jan 04 2017 Tom Eastep tom@shorewall.net
- Updated to 5.1.0-0base
* Wed Dec 28 2016 Tom Eastep tom@shorewall.net
- Updated to 5.1.0-0RC2
* Tue Dec 20 2016 Tom Eastep tom@shorewall.net
- Updated to 5.1.0-0RC1
* Mon Dec 12 2016 Tom Eastep tom@shorewall.net
- Updated to 5.1.0-0Beta2
* Sun Dec 04 2016 Tom Eastep tom@shorewall.net
- Updated to 5.1.0-0Beta1
* Fri Dec 02 2016 Tom Eastep tom@shorewall.net
- Updated to 5.0.15-0base
* Thu Dec 01 2016 Tom Eastep tom@shorewall.net
- Updated to 5.0.15-0RC2
* Sun Nov 27 2016 Tom Eastep tom@shorewall.net
- Updated to 5.0.15-0RC1
* Thu Nov 17 2016 Tom Eastep tom@shorewall.net
- Updated to 5.0.15-0Beta2
* Sun Nov 06 2016 Tom Eastep tom@shorewall.net
- Updated to 5.0.15-0Beta1
* Mon Oct 31 2016 Tom Eastep tom@shorewall.net
- Updated to 5.0.14-0RC3
* Sat Oct 29 2016 Tom Eastep tom@shorewall.net
- Updated to 5.0.14-0RC2
* Thu Oct 27 2016 Tom Eastep tom@shorewall.net
- Updated to 5.0.14-0RC1
* Tue Oct 25 2016 Tom Eastep tom@shorewall.net
- Updated to 5.0.14-0Beta2
* Sun Oct 16 2016 Tom Eastep tom@shorewall.net
- Updated to 5.0.14-0Beta1
* Sun Oct 16 2016 Tom Eastep tom@shorewall.net
- Updated to 5.0.13-0base
* Sun Oct 16 2016 Tom Eastep tom@shorewall.net
- Updated to 5.0.13-0RC2
* Sun Oct 09 2016 Tom Eastep tom@shorewall.net
- Updated to 5.0.13-0RC1
* Tue Oct 04 2016 Tom Eastep tom@shorewall.net
- Updated to 5.0.13-0Beta2
* Sun Oct 02 2016 Tom Eastep tom@shorewall.net
- Updated to 5.0.13-0Beta1
* Sat Oct 01 2016 Tom Eastep tom@shorewall.net
- Updated to 5.0.12-0base
* Sat Oct 01 2016 Tom Eastep tom@shorewall.net
- Updated to 5.0.12-0RC3
* Tue Sep 27 2016 Tom Eastep tom@shorewall.net
- Updated to 5.0.12-0RC2
* Tue Sep 20 2016 Tom Eastep tom@shorewall.net
- Updated to 5.0.12-0RC1
* Tue Sep 13 2016 Tom Eastep tom@shorewall.net
- Updated to 5.0.12-0Beta2
* Sat Aug 13 2016 Tom Eastep tom@shorewall.net
- Updated to 5.0.12-0Beta1
* Sat Aug 06 2016 Tom Eastep tom@shorewall.net
- Updated to 5.0.11-0base
* Sat Jul 30 2016 Tom Eastep tom@shorewall.net
- Updated to 5.0.11-0RC1
* Wed Jul 27 2016 Tom Eastep tom@shorewall.net
- Updated to 5.0.11-0Beta2
* Tue Jul 19 2016 Tom Eastep tom@shorewall.net
- Updated to 5.0.11-0Beta1
* Fri Jul 08 2016 Tom Eastep tom@shorewall.net
- Updated to 5.0.10-1
* Sat Jun 25 2016 Tom Eastep tom@shorewall.net
- Updated to 5.0.10-0base
* Tue Jun 21 2016 Tom Eastep tom@shorewall.net
- Updated to 5.0.10-0RC1
* Tue Jun 14 2016 Tom Eastep tom@shorewall.net
- Updated to 5.0.10-0Beta2
* Mon Jun 06 2016 Tom Eastep tom@shorewall.net
- Updated to 5.0.10-0Beta1
* Thu May 12 2016 Tom Eastep tom@shorewall.net
- Updated to 5.0.9-0base
* Thu May 05 2016 Tom Eastep tom@shorewall.net
- Updated to 5.0.9-0RC1
* Thu Apr 28 2016 Tom Eastep tom@shorewall.net
- Updated to 5.0.9-0Beta2
* Mon Apr 18 2016 Tom Eastep tom@shorewall.net
- Updated to 5.0.9-0Beta1
* Fri Apr 15 2016 Tom Eastep tom@shorewall.net
- Updated to 5.0.8-0RC2
* Mon Apr 11 2016 Tom Eastep tom@shorewall.net
- Updated to 5.0.8-0RC1
* Thu Apr 07 2016 Tom Eastep tom@shorewall.net
- Updated to 5.0.8-0Beta4
* Sat Apr 02 2016 Tom Eastep tom@shorewall.net
- Updated to 5.0.8-0Beta3
* Fri Apr 01 2016 Tom Eastep tom@shorewall.net
- Updated to 5.0.8-0Beta2
* Sun Mar 27 2016 Tom Eastep tom@shorewall.net
- Updated to 5.0.8-0Beta1
* Thu Mar 24 2016 Tom Eastep tom@shorewall.net
- Updated to 5.0.7-0base
* Fri Mar 18 2016 Tom Eastep tom@shorewall.net
- Updated to 5.0.7-0RC1
* Sun Mar 13 2016 Tom Eastep tom@shorewall.net
- Updated to 5.0.7-0Beta4
* Sun Mar 13 2016 Tom Eastep tom@shorewall.net
- Updated to 5.0.7-0Beta3
* Tue Mar 08 2016 Tom Eastep tom@shorewall.net
- Updated to 5.0.7-0Beta2
* Sat Mar 05 2016 Tom Eastep tom@shorewall.net
- Updated to 5.0.7-0Beta1
* Fri Mar 04 2016 Tom Eastep tom@shorewall.net
- Updated to 5.0.6-0Beta6
* Fri Mar 04 2016 Tom Eastep tom@shorewall.net
- Updated to 5.0.6-0Beta5
* Thu Mar 03 2016 Tom Eastep tom@shorewall.net
- Updated to 5.0.6-0Beta4
- Updated to 5.0.6-0RC1
* Sat Feb 27 2016 Tom Eastep tom@shorewall.net
- Updated to 5.0.6-0Beta3
* Sun Feb 21 2016 Tom Eastep tom@shorewall.net
- Updated to 5.0.6-0Beta2
* Fri Feb 19 2016 Tom Eastep tom@shorewall.net
- Updated to 5.0.6-0Beta1
* Wed Feb 17 2016 Tom Eastep tom@shorewall.net
- Updated to 5.0.5-0base
* Mon Feb 15 2016 Tom Eastep tom@shorewall.net
- Updated to 5.0.5-0RC2
* Wed Feb 03 2016 Tom Eastep tom@shorewall.net
- Updated to 5.0.5-0RC1
* Fri Jan 29 2016 Tom Eastep tom@shorewall.net
- Updated to 5.0.5-0Beta2
* Wed Jan 20 2016 Tom Eastep tom@shorewall.net
- Updated to 5.0.5-0Beta1
* Wed Jan 20 2016 Tom Eastep tom@shorewall.net
- Updated to 5.0.4-0base
* Tue Jan 19 2016 Tom Eastep tom@shorewall.net
- Updated to 5.0.4-0RC2
* Mon Jan 11 2016 Tom Eastep tom@shorewall.net
- Updated to 5.0.4-0RC1
* Tue Jan 05 2016 Tom Eastep tom@shorewall.net
- Updated to 5.0.4-0Beta2
* Sat Jan 02 2016 Tom Eastep tom@shorewall.net
- Updated to 5.0.4-0Beta1
* Sun Dec 27 2015 Tom Eastep tom@shorewall.net
- Updated to 5.0.3-0base
* Thu Dec 24 2015 Tom Eastep tom@shorewall.net
- Updated to 5.0.3-0RC2
* Sun Dec 13 2015 Tom Eastep tom@shorewall.net
- Updated to 5.0.3-0RC1
* Sat Dec 05 2015 Tom Eastep tom@shorewall.net
- Updated to 5.0.3-0Beta2
* Sat Nov 28 2015 Tom Eastep tom@shorewall.net
- Updated to 5.0.3-0Beta1
* Sat Nov 21 2015 Tom Eastep tom@shorewall.net
- Updated to 5.0.2-1
* Sat Nov 07 2015 Tom Eastep tom@shorewall.net
- Updated to 5.0.2-0base
* Sun Nov 01 2015 Tom Eastep tom@shorewall.net
- Updated to 5.0.2-0RC1
* Mon Oct 26 2015 Tom Eastep tom@shorewall.net
- Updated to 5.0.2-0Beta2
* Mon Oct 26 2015 Tom Eastep tom@shorewall.net
- Updated to 5.0.2-0Beta1
* Tue Oct 13 2015 Tom Eastep tom@shorewall.net
- Updated to 5.0.1-1
* Mon Oct 12 2015 Tom Eastep tom@shorewall.net
- Updated to 5.0.1-0base
* Sat Oct 03 2015 Tom Eastep tom@shorewall.net
- Updated to 5.0.0-0base
* Mon Sep 21 2015 Tom Eastep tom@shorewall.net
- Updated to 5.0.0-0RC1
* Thu Sep 10 2015 Tom Eastep tom@shorewall.net
- Updated to 5.0.0-0Beta2
* Mon Jul 27 2015 Tom Eastep tom@shorewall.net
- Updated to 5.0.0-0Beta1
